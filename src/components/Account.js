import React, { Component } from 'react';
import firebase from 'firebase';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { loginUser, logoutUser } from '../actions/UserActions';
import './Account.css';

class Account extends Component {

  constructor() {
    super();
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.errorModal = this.errorModal.bind(this);
    this.state = {
      errorMsg: ''
    };
  }

  render() {
    return (
      <ul className="navbar-nav">
        {this.getRender()}
        {this.errorModal()}
      </ul>
    );
  }

  getRender() {
    if (this.props.id) {
      return (
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle active" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={() => { return () => { window.$('.dropdown-toggle').dropdown(); } }}><img className="profile-img" src={this.props.img}></img>{this.props.name}</a>
          <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <Link className="dropdown-item" to="/profile">Profile</Link>
            <Link className="dropdown-item" to="/profile/edit">Settings</Link>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item" href="" onClick={this.handleLogout}>Cerrar sesión</a>
            </div>
        </li>
      );
    } else {
      return (
        <li className="nav-item">
          <p className="nav-link" id="loginbtn" onClick={this.handleLogin}>Log In</p>
        </li>
      );
    }
  }

  errorModal() {
    return (
      <div className="modal fade" id="accErrorModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">Error</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {this.state.errorMsg}
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleLogin(e) {
    e.preventDefault();
    let provider = new firebase.auth.GoogleAuthProvider();
    const vm = this;

    firebase.auth().signInWithPopup(provider)
      .then(res => {
        console.log(res);
        this.props.loginUser({
          id: res.user.uid,
          name: res.user.displayName,
          img: res.user.photoURL
        });
        firebase.database().ref('users').child(res.user.uid).set({
          displayName: res.user.displayName,
          email: res.user.email,
          photoURL: res.user.photoURL,
        });
        this.props.history.push('/profile');
      }).catch(err => {
        window.$('#accErrorModal').modal('hide');
        this.setState({
          errorMsg: err.message
        });
        window.$('#accErrorModal').modal('show');
      });
  }

  handleLogout() {
    firebase.auth().signOut()
      .then(res => {
        this.props.logoutUser();
      });
  }
}

const mapStateToProps = (state) => {
  return {
    id: state.id,
    name: state.name,
    img: state.img
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginUser: (user) => {
      dispatch(loginUser(user));
    },
    logoutUser: () => {
      dispatch(logoutUser());
    }
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Account));