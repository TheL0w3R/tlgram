import React, { Component } from 'react';

class NavDropdown extends Component {

  

  render() {
    return (
      <a className="nav-link dropdown-toggle" onClick={() => this.setState({show: !this.state.show})}>{this.props.text}</a>
    );
  }
}

export default NavDropdown;