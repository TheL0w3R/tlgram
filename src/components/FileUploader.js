import React, { Component } from 'react';
import firebase from 'firebase';
import { Redirect } from 'react-router-dom';

class FileUploader extends Component {
  constructor() {
    super();
    this.uploadPic = this.uploadPic.bind(this);
    this.errorModal = this.errorModal.bind(this);
    this.state = {
      uploadProgress: 0,
      errorMsg: '',
      doneUploading: false
    }
  }

  render() {
    return (
      <div>
        <div className="progress">
          <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style={{ width: this.state.uploadProgress + "%" }}></div>
        </div>
        <input type="file" ref="fileInput" className="form-control-file" accept="image/*" onChange={this.uploadPic} />
        {this.errorModal()}
        {this.redirect()}
      </div>
    );
  }

  redirect() {
    if (this.state.doneUploading) {
      return (<Redirect to="/profile" />);
    }
  }

  errorModal() {
    return (
      <div className="modal fade" id="fuErrorModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">Error</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {this.state.errorMsg}
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-info" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  uploadPic(e) {
    const file = e.target.files[0];
    const storageRef = firebase.storage().ref(`/pics/${file.name}`);
    const task = storageRef.put(file);
    let fileReader = new FileReader();
    fileReader.onloadend = (e) => {
      let arr = (new Uint8Array(e.target.result)).subarray(0, 4);
      let header = ""
      for (let i = 0; i < arr.length; i++) {
        header += arr[i].toString(16);
      }
      console.log(header);
      let matching = false;
      switch (header) {
        case '89504e47':
          //PNG
          matching = true;
          break;
        case "ffd8ffe0":
        case "ffd8ffe1":
        case "ffd8ffe2":
          //JPG OR ANY OTHER COMPATIBLE FILES  
          matching = true;
          break;
        default:
          window.$('#fuErrorModal').modal('hide');
          this.setState({
            errorMsg: 'The file you selected is not a valid image.'
          });
          window.$('#fuErrorModal').modal('show');
          this.refs.fileInput.value = '';
          break;
      }
      if (matching) {
        task.on(firebase.storage.TaskEvent.STATE_CHANGED, snapshot => {
          let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          this.setState({
            uploadProgress: percentage
          });
        }, error => {
          window.$('#fuErrorModal').modal('hide');
          this.setState({
            errorMsg: error.message
          });
          window.$('#fuErrorModal').modal('show');
        }, () => {
          this.setState({
            uploadProgress: 0
          });
          this.refs.fileInput.value = '';
          let pushRef = firebase.database().ref('pics').push({
            uid: this.props.user.id,
            pic: task.snapshot.downloadURL
          });
          this.setState({
            doneUploading: true
          });
        });
      }  
    }
    fileReader.readAsArrayBuffer(file);
  }
}

export default FileUploader;