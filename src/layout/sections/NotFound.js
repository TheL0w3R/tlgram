import React, { Component } from 'react';
import { Helmet } from 'react-helmet';

class NotFound extends Component {
  render() {
    return (
      <div className="NotFound">
        <Helmet>
          <title>Error 404</title>
        </Helmet>  
        <h1 className="text-center">404</h1>
        <p className="text-center">Oops, this page doesn't exist.</p>
      </div>
    );
  }
}

export default NotFound;