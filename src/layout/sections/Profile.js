import React, { Component } from 'react';
import firebase from 'firebase';
import { Helmet } from 'react-helmet';
import './Profile.css';
import Header from '../Header';

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      pics: {},
      user: {}
    }
  }

  componentWillMount() {
    firebase.database().ref('pics')
      .orderByChild('uid')
      .equalTo(this.props.match.params.id)
      .once('value').then(value => {
        this.setState({
          pics: {
            ...this.state.pics,
            ...value.val()
          }
        });
      });
    firebase.database().ref('users')
      .orderByKey()
      .equalTo(this.props.match.params.id)
      .once('value')
      .then(val => {
        Object.entries(val.val()).map(user => {
          this.setState({
            user: user[1]
          })
        });
      });
  }

  componentWillReceiveProps() {
    this.setState({
      pics: {}
    });
    firebase.database().ref('pics')
      .orderByChild('uid')
      .equalTo(this.props.match.params.id)
      .once('value').then(value => {
        this.setState({
          pics: {
            ...this.state.pics,
            ...value.val()
          }
        });
      });
    firebase.database().ref('users')
      .orderByKey()
      .equalTo(this.props.match.params.id)
      .once('value')
      .then(val => {
        Object.entries(val.val()).map(user => {
          this.setState({
            user: user[1]
          })
        });
      });
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>{this.state.user.displayName + ' | TLGram'}</title>
        </Helmet>
        <div className="Profile">
          <div className="header">
            <img src={this.state.user.photoURL} />
            <div className="desc">
              <h2>{this.state.user.displayName}</h2>
            </div>
          </div>
          <div className="pics">
            {
              Object.entries(this.state.pics).map(pic => (
                <div className="profile_photo" key={pic[0]}>
                  <img src={pic[1].pic} />
                </div>
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

export default Profile;