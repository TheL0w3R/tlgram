import React, { Component } from 'react';
import firebase from 'firebase';
import { withRouter } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import './Search.css';

class Search extends Component {

  constructor() {
    super();
    this.state = {
      queries: {}
    }
    this.query = this.query.bind(this);
  }

  render() {
    return (
      <div className="Search">
        <Helmet>
          <title>Search | TLGram</title>  
        </Helmet>  
        <input type="search" className="form-control" placeholder="Search..." onChange={this.query} />
        <ul className="list-group">
          {
            //val[1].displayName
            //val[1].email
            //val[1].photoURL
            (this.state.queries !== null) &&
              Object.entries(this.state.queries).map(val => (
              <li key={val[0]} className="list-group-item list-group-item-action suggestion" onClick={() => this.props.history.push('/profiles/' + val[0])}>
                <img src={val[1].photoURL} />
                <span>{val[1].displayName}</span>
              </li>
            ))
          }
        </ul>
      </div>
    );
  }

  query(e) {
    if (e.target.value !== '') {
      firebase.database().ref('users').orderByChild('displayName').startAt(e.target.value).endAt(e.target.value + '\uf8ff').once('value').then(val => {
        //console.log(val.val());
        this.setState({
          queries: val.val()
        });
      });
    } else {
      this.setState({
        queries: {}
      });
    }
  }

}

export default withRouter(Search);