import React, { Component } from 'react';
import { connect } from 'react-redux';
import FileUploader from '../../components/FileUploader';

class UploadPic extends Component {
  render() {
    return (
      <div>
        <FileUploader user={this.props.user} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: {
      id: state.id,
      name: state.name,
      img: state.img
    }
  };
};

export default connect(mapStateToProps, null)(UploadPic);