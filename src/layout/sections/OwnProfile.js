import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import './Profile.css';

class OwnProfile extends Component {
  render() {
    console.log(this.props);
    return (
      <div className="Profile">
        <Helmet>
          <title>Your Profile | TLGram</title>
        </Helmet>  
        <div className="header">
          <img src={this.props.user.img} />
          <div className="desc">
            <h2>{this.props.user.name}</h2>
            <div className="btns">
              <Link to="/upload" className="btn btn-info">Upload Photo</Link>
              <Link to="/upload" className="btn btn-warning">Edit Profile</Link>
            </div>
          </div>
        </div>
        <div className="pics">
          {
            this.props.user.pics.map(pic => (
              <div className="profile_photo" key={pic.key}>
                <img src={pic.pic} />
              </div>
            ))
          }
        </div>
      </div>
    );
  }
}

export default OwnProfile;