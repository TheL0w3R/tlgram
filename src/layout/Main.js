import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
//import firebase from 'firebase';
import { connect } from 'react-redux';
import Home from './sections/Home';
import Profile from './sections/Profile';
import NotFound from './sections/NotFound';
import OwnProfile from './sections/OwnProfile';
import UploadPic from './sections/UploadPic';
import EditProfile from './sections/EditProfile';
import Search from './sections/Search';

class Main extends Component {
  render() {
    return (
      <div className="container">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/profile" render={(props) => ((!this.props.user.id) ? (
              <Redirect to="/" />
            ) : (
              <OwnProfile {...props} user={this.props.user}/>
            ))}
          />
          <Route exact path="/upload" render={() => ((!this.props.user.id) ? (
              <Redirect to="/" />
            ) : (
              <UploadPic />  
            ))}
          />
          <Route path="/profiles/:id" render={(props) => (
            <Profile {...props} user={this.props.user} />
          )} />
          <Route exact path="/profile/edit" component={EditProfile} />
          <Route exact path="/search" component={Search} />
          <Route path="*" component={NotFound} />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: {
      id: state.id,
      img: state.img,
      name: state.name,
      pics: state.pics
    }
  };
};

export default withRouter(connect(mapStateToProps, null)(Main));