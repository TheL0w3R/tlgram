import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Account from '../components/Account';

class Header extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-md navbar-dark bg-info navbar-fixed">
        <NavLink to="/" className="navbar-brand">TLGram</NavLink>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <NavLink to="/" className="nav-link" exact activeClassName="active">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/search" className="nav-link" exact activeClassName="active">Search</NavLink>
            </li>
          </ul>
          <Account></Account>
        </div>
      </nav>
    );
  }
}

export default Header;