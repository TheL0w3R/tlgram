import { createStore, combineReducers } from 'redux';
import UserReducer from './reducers/UserReducer';

export default createStore(
  UserReducer
);