const UserReducer = (state = {
  pics: []
}, action) => {
  switch (action.type) {
    case "USER_LOGIN":
      state = {
        ...state,
        id: action.payload.id,
        name: action.payload.name,
        img: action.payload.img,
      };
      break;
    case "USER_LOGOUT":
      state = {
        pics: []
      };
      break;
    case "USER_UPLOAD_PIC":
      state = {
        ...state,
        pics: [
          ...state.pics,
          action.payload
        ]
      };
      break;
  }
  return state;
};

export default UserReducer;