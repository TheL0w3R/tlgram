import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter } from 'react-router-dom';
import firebase from 'firebase';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store from './store';

window.jQuery = window.$ = require('jquery/dist/jquery');
window.Popper = require('popper.js/dist/esm/popper');
require('bootstrap/dist/js/bootstrap.js');

firebase.initializeApp({
  apiKey: "AIzaSyARsyT0Wua0YEjO2WBQ5ewc-F54L1AZnMM",
  authDomain: "tlgram-9816c.firebaseapp.com",
  databaseURL: "https://tlgram-9816c.firebaseio.com",
  projectId: "tlgram-9816c",
  storageBucket: "tlgram-9816c.appspot.com",
  messagingSenderId: "1069500674466"
});

ReactDOM.render((
  <Provider store={store}>
    <HashRouter>
      <App />
    </HashRouter>
  </Provider>
), document.getElementById('root'));

registerServiceWorker();
