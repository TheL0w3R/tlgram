export function loginUser(user) {
  return {
    type: "USER_LOGIN",
    payload: user
  };
}

export function logoutUser() {
  return {
    type: "USER_LOGOUT",
    payload: null
  };
}

export function addPics(pic) {
  return {
    type: "USER_UPLOAD_PIC",
    payload: pic
  };
}

export function updateInfo(user) {
  
}