import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import firebase from 'firebase';
import { connect } from 'react-redux';
import { loginUser, addPics } from './actions/UserActions';
import { Helmet } from 'react-helmet';
import Header from './layout/Header';
import Main from './layout/Main';

class App extends Component {
  componentWillMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.props.loginUser({
          id: user.uid,
          name: user.displayName,
          img: user.photoURL
        });
        firebase.database().ref('pics').on('child_added', snapshot => {
          if (user.uid === snapshot.val().uid) {
            this.props.addPics(
              {
                key: snapshot.key,
                ...snapshot.val()
              }
            );
          }
        });
      }
    });
  }
  render() {
    return (
      <div id="App">
        <Helmet>
          <title>Home | TLGram</title>
        </Helmet>  
        <Header />
        <Main />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: {
      id: state.id,
      name: state.name,
      img: state.img,
      pics: state.pics
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginUser: (user) => {
      dispatch(loginUser(user));
    },
    addPics: (pics) => {
      dispatch(addPics(pics));
    }
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
